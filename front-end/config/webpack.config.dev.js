const webpack = require('webpack');
const path = require('path');


const config = {
  entry: '../',
  output: {
    chunkFilename: '[name]-[chunkhash].min.js',
    filename: '[name].min.js',
    path: path.resolve(__dirname, 'app/assets/js'),
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: 'babel-loader',
      },
    ],
  },
  plugins: [
  ],
};

module.export = config;
