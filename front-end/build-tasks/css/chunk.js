// -> Chunk for IE errors

module.exports = (paths, gulp, plugins) => {
  const csswring = require('csswring');

  return () => {
    gulp.src(`${paths.dist}/assests/css/styles.css`)
      .pipe(plugins.rename({ // -> IE rename
        suffix: '-legacy',
      }))
      .pipe(plugins.bless({ // -> Chunk css
        suffix: '-',
      }))
      .pipe(plugins.postcss([ // -> Minify
        csswring(),
      ]))
      .pipe(gulp.dest(`${paths.dist}/assets/css`))
      .pipe(gulp.dest(`${paths.src}/assets/css`));
  };
};
