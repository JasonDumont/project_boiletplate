// -> Main css processing

module.exports = (paths, gulp, plugins) => {
  const autoprefixer = require('autoprefixer');
  const csswring = require('csswring');
  const mqpacker = require('css-mqpacker');

  const options = {
    style: 'expanded',
    // includePaths: [] // -> Alter to include fontawesome etc
  };

  return () => {
    gulp.src(`${paths.app}/assets/scss/*.scss`)
      .pipe(plugins.sourcemaps.init()) // -> generate sourcemaps
      .pipe(plugins.sass(options)
        .on('error', plugins.sass.logError))
      .pipe(plugins.postcss([
        autoprefixer({
          browsers: [ '> 2%', 'IE >= 8', 'iOS >=7' ],
          cascade: false,
          map: true,
          remove: true,
        }),
        csswring({
          removeAllComments: true,
        }),
        mqpacker({
          sort: true,
        }),
      ]))
      .pipe(plugins.sourcemaps.write('.'))
      .pipe(gulp.dest(`${paths.dist}/assets/css`))
      .pipe(gulp.dest(`${paths.src}/assets/css`))

      .pipe(plugins.filter('**/*.css'))
      .pipe(plugins.browserSync.stream());
  };
};
