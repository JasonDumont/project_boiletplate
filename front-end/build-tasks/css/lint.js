// -> CSS linting

module.exports = (paths, gulp, plugins) => {
  return () => {
    return gulp.src(`${paths.app}/assets/scss/**/*.scss`)
      .pipe(plugins.stylelint({
        failAfterError: false,
        reporters: [
          {
            formatter: 'string',
            console: true,
          },
        ],
      }));
  };
};
