'use strict';

// -> Collect file contents

module.exports = (path) => {
  let content = '';
  
  try {
    content = require('fs').readFileSync(path, 'utf8');
  } catch (err) {
    console.log('Something went wrong!: ' + err);
  };

  return content.trim(); // -> Return the gathered file
}
