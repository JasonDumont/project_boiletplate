// -> Build out html files

module.exports = (paths, gulp, plugins) => {
  
  const nunjucks = require('nunjucks'); // -> Templating language

  const options = {
    env: new nunjucks.Environment(new nunjucks.FileSystemLoader([ `${paths.app}/templates` ], { noCache: true }))
  };

  const data = {
    timeStamp: Date.now(),
    paths,
  };

  // -> Collection content
  options.env.addFilter('outputFileContent', require('./filter/file-content'));

  return () => {
    return gulp.src([
      `${paths.app}/templates/*.html`,
    ])
      .pipe(plugins.nunjucks.compile(data, options))
      .pipe(gulp.dest(paths.dist))
      .pipe(plugins.filter('**/*.html'))
      .pipe(plugins.browserSync.stream());
  };
};
