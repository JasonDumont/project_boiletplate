// -> Accessbility checker

module.exports = (paths, gulp, plugins) => {

  const options = { // -> Define settings
    accessibilityLevel: 'WCAG2AA',
    force: true,
    reportLevels: {
      notice: false,
      warning: false,
      error: true
    }
  };

  return (callback) => {
    return gulp.src(`${paths.dist}/*.html`)
      .pipe(plugins.accessibility(options))
      .on ('error', console.log(err));
  };
};
