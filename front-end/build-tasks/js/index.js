// -> Webpack bundle
const config = require('../../config/webpack.config.dev');

module.exports = (paths, gulp, plugins) => {

  // Child modules
  const stream = require('webpack-stream');
  const webpack = require('webpack');
  const named = require('vinyl-named');


  return () => {

    // Process entry point
    return gulp.src(`${paths.app}/assets/js/*.js`)
      .pipe(named())
      .pipe(stream(config, webpack))
      .pipe(gulp.dest(`${paths.dist}/assets/js`))
      .pipe(gulp.dest(`${paths.src}/assets/js`));
  };
};

