// -> Generate a modernizr file

module.exports = (paths, themes, gulp, plugins) => {

  const fs = require('fs');
  const modernizr = require('modernizr');

  return (callback) => {
    modernizr.build({
      "feature-detects": [ 'css/flexbox' ],
      "options": [ 'setClasses' ]
    }, (result) => { // -> Once complete write the file
      fs.writeFileSync(`${paths.app}/assets/js/vendor/modernizr.js`, `/* eslint-disable */\n${result}`);
      callback();
    });
  };
};
