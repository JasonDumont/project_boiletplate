// -> Lint through JS airbnb standards

module.exports = (paths, gulp, plugins) => {
  return () => {
    return gulp.src(`${paths.app}/assets/js/**/*.js`)
      .pipe(plugins.eslint())
      .pipe(plugins.eslint.format());
  };
};
