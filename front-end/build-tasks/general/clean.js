// -> Clean files

module.exports = (paths, gulp, plugins) => {
  return (callback) => {
    return require('del')([paths.dist, paths.src], callback);
  };
};
