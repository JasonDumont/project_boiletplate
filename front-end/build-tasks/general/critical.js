// -> Render out a critical Partial

module.exports = (paths, gulp, plugins) => {
  const fs = require('fs');

  return () => {
    generateCritical(fs, paths);
  };
};

function generateCritical(fs, paths) {
  const critJSPath = './front-end/dist/assets/js/common.js';
  const critCSSPath = './front-end/dist/assets/css/critical.css';
  const criticalJS = fs.readFileSync(critJSPath);
  const criticalCSS = fs.readFileSync(critCSSPath);


  const criticalPartial = `
    Some umbraco text up here about the partial view piece
    another line 
    {
      template master = 'master.cshtml'
    }
    <!-- Critical CSS -->
    <!--[if (gte IE 9)|!(IE)]><!-->
    <style type="text/css">
      ${criticalCSS}
    </style>
    <!--<![endif]-->
    <!-- Critical JS -->
    <script>${criticalJS}</script>
    <!-- Lazy-load CSS -->
    <!--[if (gte IE 9)|!(IE)]><!-->
    <script>loadCSS('assets/css/styles.css');</script>
    <noscript><link rel="stylesheet" href="assets/css/styles.css"></noscript>
    <!--<![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="assets/css/styles.css">
    <![endif]-->
  `;

  fs.appendFile('./src/assets/criticalLoad.cshtml', criticalPartial, { flag: 'w' }, (error) => {
    if (error) {
      console.log(error);
    }
  });
}
