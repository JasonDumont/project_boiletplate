// -> Image optimisation

module.exports = (paths, gulp, plugins) => {

  // Child modules
  const pngquant = require('imagemin-pngquant');
  const mozjpeg = require('imagemin-jpegtran');

  // Return module
  return () => {

    // Override plugins (default + pngquant, mozjpeg)
    const use = [
      plugins.imagemin.gifsicle(),
      plugins.imagemin.svgo(),
      pngquant(),
      mozjpeg({
        quality: 70,
        progressive: true,
      }),
    ];

    return gulp.src(`${paths.app}/assets/images/**/*.{png,jpg,gif,svg}`)
      .pipe(plugins.imagemin(use))
      .pipe(gulp.dest(`${paths.dist}/assets/images`))
      .pipe(gulp.dest(`${paths.src}/assets/images`));
  };
};

