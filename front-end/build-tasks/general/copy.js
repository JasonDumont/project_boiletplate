// -> Copy files

module.exports = (paths, gulp, plugins) => {
  return () => {
    return gulp.src(`${paths.app}/assets/**`)
      .pipe(plugins.newer(`${paths.app}/assets`))
      .pipe(gulp.dest(`${paths.dist}/assets`))
      .pipe(gulp.dest(`${paths.src}/assets`))
      .pipe(plugins.preservetime());
  };
};

