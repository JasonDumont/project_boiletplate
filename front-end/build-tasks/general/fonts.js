// -> Copy files

module.exports = (paths, gulp, plugins) => {
  return () => {
    return gulp.src(`${paths.app}/assets/fonts/**`)
      .pipe(gulp.dest(`${paths.dist}/assets/fonts`))
      .pipe(gulp.dest(`${paths.src}/assets/fonts`));
  };
};

