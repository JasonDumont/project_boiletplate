// -> Compile all the content

// -> Core libraries/data
  const gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    config = require('./package.json').config.paths;

  // -> Load in libraries dynamically
  plugins.browserSync = require('browser-sync').create(); 
  plugins.sequence = require('run-sequence'); 
  plugins.stream = require('event-stream'); 
  plugins.combine = require('stream-combiner');

//

// -> Gather individual tasks
  function GatherTask(task) {
    let { build } = config; // -> Deconstruct build tasks source
    return require(`${build}/${task}`)(config, gulp, plugins); // -> return the sepecific file
  }
//

// -> MODULE GATHERING
  // -> HTML
  gulp.task('html', GatherTask('html/index'));
  gulp.task('a11y', GatherTask('html/a11y'));
  gulp.task('html-lint', GatherTask('html/lint'));

  // -> CSS
  gulp.task('css', GatherTask('css/index'));
  gulp.task('css-lint', GatherTask('css/lint'));
  gulp.task('css-chunk', GatherTask('css/chunk'));
  gulp.task('css-clean', GatherTask('css/clean'));

  // -> JS
  gulp.task('js', GatherTask('js/index'));
  gulp.task('js-lint', GatherTask('js/lint'));
  gulp.task('modernizr', GatherTask('js/modernizr'));

  // -> GENERAL
  gulp.task('clean', GatherTask('general/clean'));
  gulp.task('copy', GatherTask('general/copy'));
  gulp.task('images', GatherTask('general/images'));
  gulp.task('serve', GatherTask('general/serve'));
  gulp.task('watch', GatherTask('general/watch'));
  gulp.task('critical', GatherTask('general/critical'));
  gulp.task('fonts', GatherTask('general/fonts'));
//

// -> BUILD PROCESSES
  // -> Developing build process
  gulp.task('development', function (callback) {
    return plugins.sequence('build', 'copy', 'fonts', ['watch', 'serve'], callback);
  });
  
  // -> a11y based build
  gulp.task('a11y', function (callback) {
    return plugins.sequence('build', 'copy', 'html-a11y', callback);
  });
  

  // -> Default build process task
  gulp.task('default', function (callback) {
    return plugins.sequence('build', 'copy', callback);
  });

  gulp.task('build', ['clean'], function (callback) {
    return plugins.sequence(['js-lint', 'css-lint'], 'modernizr', ['js', 'css', 'images'], 'html', ['css-chunk', 'html-lint'], 'css-clean', 'critical', callback);
  });
//
